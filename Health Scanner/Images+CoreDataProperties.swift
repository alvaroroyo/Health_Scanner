//
//  Images+CoreDataProperties.swift
//  Health Scanner
//
//  Created by Alvaro Royo on 19/4/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import Foundation
import CoreData


extension Images {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Images> {
        return NSFetchRequest<Images>(entityName: "Images")
    }

    @NSManaged public var img: NSData?

}
