//
//  Alergens+CoreDataProperties.swift
//  Health Scanner
//
//  Created by Alvaro Royo on 19/4/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import Foundation
import CoreData


extension Alergens {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Alergens> {
        return NSFetchRequest<Alergens>(entityName: "Alergens")
    }

    @NSManaged public var id: NSNumber?
    @NSManaged public var name: String?
    @NSManaged public var img: Images?
    @NSManaged public var products: NSSet?

}

// MARK: Generated accessors for products
extension Alergens {

    @objc(addProductsObject:)
    @NSManaged public func addToProducts(_ value: Products)

    @objc(removeProductsObject:)
    @NSManaged public func removeFromProducts(_ value: Products)

    @objc(addProducts:)
    @NSManaged public func addToProducts(_ values: NSSet)

    @objc(removeProducts:)
    @NSManaged public func removeFromProducts(_ values: NSSet)

}
