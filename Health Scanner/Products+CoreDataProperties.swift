//
//  Products+CoreDataProperties.swift
//  Health Scanner
//
//  Created by Alvaro Royo on 19/4/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import Foundation
import CoreData


extension Products {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Products> {
        return NSFetchRequest<Products>(entityName: "Products")
    }

    @NSManaged public var brand: String?
    @NSManaged public var ingredients: String?
    @NSManaged public var name: String?
    @NSManaged public var pvp: String?
    @NSManaged public var barcode: String?
    @NSManaged public var fav: NSNumber?
    @NSManaged public var alergens: NSSet?
    @NSManaged public var health: NSSet?
    @NSManaged public var img: Images?

}

// MARK: Generated accessors for alergens
extension Products {

    @objc(addAlergensObject:)
    @NSManaged public func addToAlergens(_ value: Alergens)

    @objc(removeAlergensObject:)
    @NSManaged public func removeFromAlergens(_ value: Alergens)

    @objc(addAlergens:)
    @NSManaged public func addToAlergens(_ values: NSSet)

    @objc(removeAlergens:)
    @NSManaged public func removeFromAlergens(_ values: NSSet)

}

// MARK: Generated accessors for health
extension Products {

    @objc(addHealthObject:)
    @NSManaged public func addToHealth(_ value: Health)

    @objc(removeHealthObject:)
    @NSManaged public func removeFromHealth(_ value: Health)

    @objc(addHealth:)
    @NSManaged public func addToHealth(_ values: NSSet)

    @objc(removeHealth:)
    @NSManaged public func removeFromHealth(_ values: NSSet)

}
