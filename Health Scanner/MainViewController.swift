//
//  MainViewController.swift
//  Health Scanner
//
//  Created by Alvaro Royo on 17/4/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import UIKit
import AVFoundation

class MainViewController: UIViewController {

    @IBOutlet weak var videoView: UIView!
    
    private var avPlayer:AVPlayer!
    private var avPlayerlayer:AVPlayerLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let theURL = Bundle.main.url(forResource: "backgroundVideo", withExtension: "mp4")
        
        self.avPlayer = AVPlayer(url: theURL!)
        self.avPlayer.volume = 0
        self.avPlayer.actionAtItemEnd = .none
        
        self.avPlayerlayer = AVPlayerLayer(player: self.avPlayer)
        self.avPlayerlayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayer.currentItem)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.avPlayerlayer.frame = self.videoView.frame
        self.videoView.layer.addSublayer(self.avPlayerlayer)
        self.avPlayer.play()
    }
    
    func playerItemDidReachEnd(notification:Notification){
        let p:AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: kCMTimeZero)
    }
    
    @IBAction func menuAction() {
    }

    @IBAction func scannerAction() {
        let scanner = ScannerViewController()
        
        self.navigationController?.pushViewController(scanner, animated: true)
    }
    
}
