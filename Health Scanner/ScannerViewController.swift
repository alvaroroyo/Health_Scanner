//
//  ScannerViewController.swift
//  Health Scanner
//
//  Created by Alvaro Royo on 18/4/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import UIKit
import AVFoundation

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    private var trueType:String!
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var scannLine: UIView!
    @IBOutlet weak var favBtn: UIButton!
    
    private var captureSession:AVCaptureSession!
    private var videoPreviewLayer:AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        
        var image = UIImage(named: "fav")
        image = image?.withRenderingMode(.alwaysTemplate)
        
        favBtn.setImage(image, for: .selected)
        
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        UIView.animateKeyframes(withDuration: 1.5, delay: 0.0, options: .repeat, animations: { 
            
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.75, animations: { 
                self.scannLine.layer.opacity = 0.2
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.75, relativeDuration: 0.75, animations: { 
                self.scannLine.layer.opacity = 1
            })
            
        }, completion: nil)
        
        do{
            
            let input = try AVCaptureDeviceInput.init(device: captureDevice)
            
            self.captureSession = AVCaptureSession()
            self.captureSession.addInput(input)
            
            self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
            self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            
            self.videoView.layer.addSublayer(self.videoPreviewLayer)
            self.videoPreviewLayer.frame = self.videoView.frame
            
            self.videoPreviewLayer.connection.videoOrientation = .portrait
            
            let captureMetadataOutput = AVCaptureMetadataOutput()
            self.captureSession.addOutput(captureMetadataOutput)
            
            let concurrentQueue = DispatchQueue(label: "SCAN_CODES_THREAD", attributes: .concurrent)
            
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: concurrentQueue)
            
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeEAN13Code]
            self.trueType = AVMetadataObjectTypeEAN13Code
            
            self.captureSession.startRunning()
            
        } catch {
            
            print("Es posible que el dispositivo no tenga cámara. O la aplicación no disponga de permisos para usarla.")
            
        }
        
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        let metadataObj = metadataObjects.first as! AVMetadataMachineReadableCodeObject
        
        if(metadataObj.type == self.trueType){
            
            let result = metadataObj.stringValue
            
            print("Code readed: %@",result!)
            
            self.captureSession.stopRunning()
            
            DispatchQueue.main.async {
                
            }
            
        }
        
    }
    
    @IBAction func fevoriteAction() {
        
        favBtn.isSelected = !favBtn.isSelected
        
    }
    
}
