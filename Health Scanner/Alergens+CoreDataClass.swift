//
//  Alergens+CoreDataClass.swift
//  Health Scanner
//
//  Created by Alvaro Royo on 19/4/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import Foundation
import CoreData


public class Alergens: NSManagedObject {

    func set(dictionary:[String:Any]){
        
        self.name = dictionary["name"] as? String
        self.id = dictionary["id"] as? NSNumber
        
        let data = NSData.init(base64Encoded: dictionary["img"] as! String)
        
        let image:Images = NSEntityDescription.insertNewObject(forEntityName: "Images", into: self.managedObjectContext!) as! Images
        image.img = data
        self.img = image
        
    }
    
}
