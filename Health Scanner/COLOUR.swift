//
//  Colours.swift
//  Health Scanner
//
//  Created by Alvaro Royo on 17/4/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import Foundation
import UIKit

class COLOUR {
    
    static let backgroundColor = UIColor.init(red: 0, green: 102.0 / 255.0, blue: 172.0 / 255.0, alpha: 1)
    
}
