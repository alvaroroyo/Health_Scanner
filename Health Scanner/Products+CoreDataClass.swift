//
//  Products+CoreDataClass.swift
//  Health Scanner
//
//  Created by Alvaro Royo on 19/4/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import Foundation
import CoreData


public class Products: NSManagedObject {

    func set(dictionary:[String:Any]){
        
        self.name = dictionary["name"] as? String
        self.brand = dictionary["brand"] as? String
        self.pvp = dictionary["pvp"] as? String
        self.barcode = dictionary["barcode"] as? String
        self.ingredients = dictionary["ingredients"] as? String
        
        let data = NSData.init(base64Encoded: dictionary["img"] as! String)
        
        let image:Images = NSEntityDescription.insertNewObject(forEntityName: "Images", into: self.managedObjectContext!) as! Images
        image.img = data
        
        self.img = image
        
        let alergensArray:[Int] = (dictionary["alergens"] as? [Int])!
        let healthArray:[Int] = (dictionary["health"] as? [Int])!
        
        for alergenId in alergensArray {
            
            let alergen = CoreDataMOC.sharedInstance.select(entity: "Alergens", predicate: NSPredicate.init(format: "id == %@", NSNumber(integerLiteral: alergenId)) , limit: 1, fault: true)?.first as! Alergens
            self.addToAlergens(alergen)
            
        }
        
        for healthId in healthArray {
            
            let health = CoreDataMOC.sharedInstance.select(entity: "Health", predicate: NSPredicate.init(format: "id == %@", NSNumber(integerLiteral: healthId)) , limit: 1, fault: true)?.first as! Health
            self.addToHealth(health)
            
        }
        
    }
    
}
